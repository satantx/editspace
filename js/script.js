$(document).ready(function() {

	
	// валидация форм
	$('.form').each(function() {
		var form = $(this),
			button = $('.form-submit', form),
			sizeError = null;

		function check() {
			$('.required', form).each(function() {
				var el = $(this),
					elVal = el.val().trim();
				elVal != '' ? el.removeClass('error') : el.addClass('error');
				sizeError = $('.error', form).length;
			});
		}
		button.click(function() {
			check();
			if (sizeError>0) {
				return false;
			} else {
				$.ajax({
					type: "POST",
					url: "/user/index.php",
					data: form.serialize()
				});
			}
		});
	
	});

});
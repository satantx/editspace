<?
    require "config.php";
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title></title>
	<meta name="description" content="">

	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="stylesheet" href="/style.css" >

    <script src="/js/jquery.js"></script>
    <script src="/js/script.js"></script>

</head>
<body>

<div class="wrapper">
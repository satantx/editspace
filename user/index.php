<?

    require '../content/header.php';

    $data = $_POST;

    if( isset($data['do_login'])) {

        $user = trim($data['login']);
        $pass = $data['password'];

        $errors = array();
        if($user == $admin_login) {
            if (password_verify($pass, $admin_pass)) {
                echo '<span>Авторизация прошла успешно<br><a href="/">На главную</a></span>';
                $_SESSION['$login_user'] = $user;
            } else {
                $errors[] = 'Неверный пароль';
            }
        } else {
            $errors[] = 'Неверный логин';
        }
    }

?>


<? if( !isset($_SESSION['$login_user'])) : ?>
    <div class="wrap">
        <h3>Авторизация</h3>
        <form method="POST" class="form">
            <p class="error-text">
                <?
                    if (!empty($errors)) {
                        echo array_shift($errors);
                    }
                ?>
            </p>
            <input type="text" value="<?= $data['login'] ?>" name="login" placeholder="Логин" class="input required" />
            <input type="password" value="" name="password" placeholder="Пароль" class="input required" />
            <button type="submit" name="do_login" class="button form-submit">Отправить</button>
        </form>
    </div>
<? endif ?>

<? require '../content/footer.php'; ?>



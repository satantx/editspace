<?
   	require 'content/header.php';
?>
<? if( isset($_SESSION['$login_user'])) : ?>
	<div class="panel">
		<div class="login-block">
			Добро пожаловать: <?= $admin_login ?> / <a href="user/logout.php">Выход</a>
		</div>
	</div>
<? endif; ?>


<div class="container">
	<p>Немецкая авиакомпания Air Berlin запустила процедуру банкротства. Основной акционер — арабская компания Etihad Airways — прекратила финансирование. Об этом говорится в сообщении авиакомпании, которое цитирует издание Spiegel.</p>

	<p>Air Berlin продолжит совершать регулярные рейсы по всем направлениям. Правительство Германии направило авиакомпании кредит в размере 150 миллионов евро на период реструктуризации. В заявлении компании указано, что никакого дополнительного финансирования она искать не будет.</p>

	<p>В течение последних трёх лет Air Berlin пыталась уйти от банкротства — убыток перевозчика составил 1,2 миллиарда евро. В сентябре 2016 года компания уволила больше тысячи человек и сократила парк самолётов на 142 единицы. В октябре 2016 года Air Berlin договорилась передать Lufthansa часть самолётов и экипажей.</p>

	<p>В 2015 году Air Berlin закрыла направление из Москвы в Вену, в 2016 году из Петербурга и Калининграда в Берлин. Сейчас на сайте перевозчика можно купить билеты из 15 городов России в Германию, Австрию, Великобританию и другие страны.</p>
</div>

<? require 'content/footer.php'; ?>






